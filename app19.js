const express = require('express');
 const path = require('path');
const bodyParser = require('body-parser');
 const app19 = express();
 
 app19.use('/public', express.static(path.join(__dirname, 'static')));
app19.use(bodyParser.urlencoded({extended: false}));
 app19.get('/', (req,res)=>{
     res.sendFile(path.join(__dirname,'static','index.html'));
 });
 
app19.post('/', (req,res)=>{
    console.log(req.body);
    res.send('Successfully posted data.');
})

 app19.listen(3000);