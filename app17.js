const express = require('express');
 const app17 = express();
 
 app17.get('/', (req,res)=>{
     res.send('Hello World');
 });
 
app17.get('/example', (req,res)=>{
   res.send('This is an example route');
});

app17.get('/example/:name/:age',(req,res)=>{
    console.log(req.params);
    console.log(req.query);
    res.send(req.params.name + " : " + req.params.age);
});

 app17.listen(3000);