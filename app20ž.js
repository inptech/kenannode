const express = require('express');
 const path = require('path');
 const bodyParser = require('body-parser');
 const app20ž = express();
 
 app20ž.use('/public', express.static(path.join(__dirname, 'static')));
 app20ž.use(bodyParser.urlencoded({extended: false}));
app20ž.use(bodyParser.json());
 app20ž.get('/', (req,res)=>{
     res.sendFile(path.join(__dirname,'static','index20'.html'));
 });
 
 app20ž.post('/', (req,res)=>{
     console.log(req.body);
    res.json({success : true});
 })
 
 app20ž.listen(3000);