app.use(express.static('./public'))
app.all('*', (req, res) => {
  res.status(404).send('<h1>resource not found</h1>')
})